FROM maven:3.3.9-jdk-8-alpine

WORKDIR /service

ADD pom.xml /service/pom.xml
ADD src /service/src

RUN mvn package

ENV AWS_REGION=us-west-1
ENV AWS_ACCESS_KEY_ID=fake
ENV AWS_SECRET_ACCESS_KEY=fake

CMD java -jar /service/target/sp-sum-service-1.0.0.jar
