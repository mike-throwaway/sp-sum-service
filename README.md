# Story Point Sum Service

Microservice that exposes a REST endpoint to sum story points from a backend Jira API search query.

This endpoint:
 - Makes a request to the search api on a configured Jira instance
 - Parses the response and counts the total story points from all issues in the request
 - Pushes a message to a configured SQS queue with a supplied description and calculated sum of points

## Development quirks and assumptions
For the sake of time-boxing, I made the following assumptions. If any of these assumptions are wrong, let me know and I will correct.

- I think the mountebank config that was supplied with this project was incorrect. That or I did something terribly wrong. Instead, I created a quick static webservice that responded with the supplied example. Unit tests cover response edge cases.
- Just to make the Java AWS SDK happy, I set a few default aws environment variables in the dockerfile. They aren't used and only for the mock SQS queue, but needed to stop startup errors caused by the AWS SDK. When running against AWS, be sure to set these variables *when running the image*
- The default JIRA url uses `localhost`, which isn't available in docker. If you are running a local jira instance while running this microservice in a local docker container, override the `JIRA_URL` environment variable. If using Docker v18 or above, the hostname `host.docker.internal` can be used
- I didn't have a running Jira instance or SQS queue. Mock services were used. I cannot 100% validate this will work the first time in a real deployment.
- See servicability notes below on the final touches that need to happen before production code is complete
- I made an assumption that the only important info in the response was if the request was successful or not. As a result, the body of the response is always empty. You can verify if the request was successful based on its response code (see list below). The logs would be used to debug non-successful reasons. I did this just as a layer of protection in case PII leaked in exception details.

## Requirements
For local testing and building, the following must be installed

- Maven 3, `3.3.9` was tested and used in the dockerfile (to run outside of Docker)
- Java 8 JDK (to run outside of Docker)
- Docker to build the final image. `18.06.1-ce, build e68fc` was tested

## Building
This microservice is deployable through a docker image. 
The dockerfile will download dependencies, run unit tests, and package the jar. It can be built by running the following in the root folder:

```
docker build -t sp-sum-service:latest .
```

Running the image will start the default spring boot application server and expose the api.


## Running
The microservice listens on port 8080. Be sure to expose this port when running the docker container. 

The following environment variables can be set:
- `JIRA_URL`: The hostname of the Jira server. For example: `http://host.docker.internal:3000`
- `QUEUE_URL`: The queue url. If using AWS, be sure to set the correct AWS environment variables. An example using the mock local SQS mock: `http://host.docker.internal:9324/queue/cst-test-queue`

Here is what I ran locally, exposing the API at port 8080:
```
docker run -it -p8080:8080 -eJIRA_URL=http://host.docker.internal:3000 -eQUEUE_URL=http://host.docker.internal:9324/queue/cst-test-queue sp-sum-service:latest
```

## API
The REST endpoint to trigger a search and store the results in SQS is 
```
<service hostname>/api/issue/sum?query=<query string>&name=<descriptive name>
```

(replace <query string\> and <descriptive name\> when running)

Response codes:
- 201 if the request was successful and a message added to the queue
- 400 if the request was poorly formed. Check that both query and name params are specified in the request and they are not empty.
- 500 if the request failed. This could be a failure between the service and Jira/SQS. Check logs for more info.

Metrics should be added, at least on 500 response codes, to alert the service owner of potential downtime.

## Note on Servicability
Unit tests are supplied and run on docker build. The docker image listens on port 8080. Healthcheck endpoint not provided, but would realistically be added before deployment. The service logs debug, warning, errors, metrics, and exceptions. For this project, I stubbed these logs to stdout. In the real world, I would update the `LoggerService` code to use the appropriate backend formats (like datadog, splunk metadata, etc)

## Tests
Unit testing occurs when building the docker image. If you want to run tests locally, and assuming you have Java 8 and Maven 3 on your local machine, you can run them manually:
```
mvn test
```