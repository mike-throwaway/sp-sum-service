package com.atlassian.cst.spsumservice;

import com.atlassian.cst.spsumservice.pojo.JiraIssues;
import com.atlassian.cst.spsumservice.services.JiraService;
import com.atlassian.cst.spsumservice.services.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@SpringBootApplication
public class SpSumServiceApplication {

    private JiraService jiraService;
    private QueueService queueService;

    @Autowired
    public SpSumServiceApplication(JiraService jira_service, QueueService queue_service) {
        jiraService = jira_service;
        queueService = queue_service;
    }

	@RequestMapping(path="/api/issue/sum", method = GET)
	public ResponseEntity<String> sum(@RequestParam("query") String query, @RequestParam("name") String descriptive_name) {

        //Check to make sure the supplied query parameters are valid
        if(query == null || descriptive_name == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(query.isEmpty() || descriptive_name.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //Perform the search, null checking the results before inserting into the queue
        JiraIssues issues = jiraService.search(query);

        if(issues != null) {
            boolean success = queueService.pushStoryPointSumToQueue(descriptive_name, issues.getTotalStoryPoints());
            if (!success) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            return new ResponseEntity<>(HttpStatus.CREATED);

        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	public static void main(String[] args) {
	    SpringApplication.run(SpSumServiceApplication.class, args);
	}

}

