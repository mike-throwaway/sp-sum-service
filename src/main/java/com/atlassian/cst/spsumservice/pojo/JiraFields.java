package com.atlassian.cst.spsumservice.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JiraFields
 * POJO providing jackson JSON support for the fields attribute in a JsonIssue.
 * Right now, only requires storyPoints attribute
 */
public class JiraFields {

    private int storyPoints;
    public int getStoryPoints() {
        return storyPoints;
    }

    @JsonCreator
    public JiraFields(@JsonProperty(value = "storyPoints", required = true) int story_points) {
        this.storyPoints = story_points;
    }

}
