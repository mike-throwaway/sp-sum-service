package com.atlassian.cst.spsumservice.pojo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * JiraIssue
 * POJO providing jackson JSON support for a Jira issue returned from the Jira API
 * Right now, only consumes the issueKey and fields attributes.
 */
public class JiraIssue {
    private String issueKey;
    private JiraFields fields;

    @JsonCreator
    public JiraIssue(@JsonProperty(value = "issueKey", required = true) String issue_key,
                      @JsonProperty(value = "fields", required = true) JiraFields fields) {
        this.issueKey = issue_key;
        this.fields = fields;
    }

    public String getIssueKey() {
        return issueKey;
    }

    /**
     * A helper function to extract story points from the fields attributes
     * @return The story points for this issue
     */
    public int getStoryPoints() {
        return fields.getStoryPoints();
    }

}