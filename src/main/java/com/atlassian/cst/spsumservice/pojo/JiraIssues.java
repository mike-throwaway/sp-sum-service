package com.atlassian.cst.spsumservice.pojo;

/**
 * JiraIssues
 * Manages a collection of JiraIssue objects, and provides the ability to count up the points
 */
public class JiraIssues {

    private JiraIssue[] issues;

    public JiraIssues(JiraIssue[] issues) {
        this.issues = issues == null ? new JiraIssue[0] : issues;
    }

    /**
     * @return total story points among all children issues
     */
    public int getTotalStoryPoints() {
        int totalPoints = 0;
        for (JiraIssue issue : issues) {
            totalPoints += issue.getStoryPoints();
        }

        return totalPoints;
    }

    /**
     * @param index index of issue in collection
     * @return JiraIssue at index
     */
    public JiraIssue get(int index) {
        return issues[index];
    }

    /**
     * @return Total issues in collection
     */
    public int size() {
        return issues.length;
    }
}