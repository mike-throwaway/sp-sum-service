package com.atlassian.cst.spsumservice.pojo;

/**
 * QueueMessageStoryPoints
 * Simple POJO used to map the descriptive name and total story points
 * into a message to be pushed into a queue
 */
public class QueueMessageStoryPoints {
    private String name;
    private int totalPoints;

    public int getTotalPoints() {
        return totalPoints;
    }

    public String getName() {
        return name;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }
    public void setName(String name) {
        this.name = name;
    }
}
