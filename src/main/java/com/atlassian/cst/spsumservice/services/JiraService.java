package com.atlassian.cst.spsumservice.services;

import com.atlassian.cst.spsumservice.pojo.JiraIssue;
import com.atlassian.cst.spsumservice.pojo.JiraIssues;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;

/**
 * JiraService
 * Service bean providing access to the Jira API for a configured Jira server.
 * Set JIRA_URL environment variable to set the correct hostname
 */
@Service
public class JiraService {
    private static final String QUEUE_URL_ENV_VAR_NAME = "JIRA_URL";
    private static final String DEFAULT_JIRA_URL = "http://localhost:3000";
    private static final String SEARCH_API_PATH = "/rest/api/2/search";

    private static final LoggerService logger = LoggerService.getInstance();

    private String getJiraApiBaseUrl() {
        String queueUrl = System.getenv(QUEUE_URL_ENV_VAR_NAME);
        if (queueUrl == null) {
            logger.warn("JIRA_URL environment variable not defined. Assuming default...");
            queueUrl = DEFAULT_JIRA_URL;
        }

        return queueUrl;
    }

    //NOTE: Only public for unit test mocking
    public String rawSearch(String query) {
        URI uri = UriComponentsBuilder
            .fromHttpUrl(getJiraApiBaseUrl())
            .path(SEARCH_API_PATH)
            .queryParam("q", query)
            .build()
            .toUri();

        logger.info(uri.toString());

        try {
            long startTime = System.nanoTime();
            ResponseEntity<String> response = new RestTemplate().getForEntity(uri, String.class);

            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;
            logger.logJiraResponse(timeElapsed, response.getStatusCodeValue());
            String body = response.getBody();

            if (response.getStatusCode().isError() || body == null) {
                logger.warn("Received invalid body or error status code from Jira... returning null");
                return null;
            }

            return body;

        } catch(Exception e) {
            logger.logJiraResponseFailure(e);
        }

        return null; //TODO: do better
    }

    /**
     * Perform a Jira search using a supplied queury and parse the response into a POJO
     * @param query The query string used in the Jira search API
     * @return JiraIssues parsed from the search API results, or null if there was an error
     */
    public JiraIssues search(String query) {

        try {
            String searchResultsJson = rawSearch(query);
            if (searchResultsJson == null) {
                return null;
            }

            JiraIssue[] jiraIssuesArray = new ObjectMapper().readValue(searchResultsJson, JiraIssue[].class);
            if (jiraIssuesArray == null) {
                return null;
            }

            return new JiraIssues(jiraIssuesArray);

        } catch (IOException e) {
            logger.exception("Search results were invalid json", e);
            return null;
        }
    }
}
