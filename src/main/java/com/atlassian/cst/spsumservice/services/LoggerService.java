package com.atlassian.cst.spsumservice.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LoggerService
 * This is a wrapper for all things message and metric logging
 * For servicability, these methods would be modified to use appropriate APIs (datadog, splunk formatting, etc)
 * Since this is just a take-home project, I will just log to console.
 */
public class LoggerService {

    private static final Logger logger = LoggerFactory.getLogger("SPSumService");

    // Designed as a singleton. Use getInstance() instead.
    private LoggerService() { }

    public void logJiraResponse(long response_ns, int response_code) {
        logger.info("Jira API Status Code: " + response_code + "; Response time: " + response_ns + "ns");
    }

    public void logJiraResponseFailure(Exception e) {
        logger.error("Jira Response Failure", e);
    }

    public void logQueueResponse(long response_ns, int response_code) {
        logger.info("Queuing Status Code: " + response_code + "; Response time: " + response_ns + "ns");
    }

    public void logQueuingException(Exception e) {
        logger.error("Queuing Failure", e);
    }

    public void info(String message) {
        logger.info(message);
    }

    public void warn(String message) {
        logger.warn(message);
    }

    public void exception(String message, Exception e) {
        logger.error(message, e);
    }

    private static LoggerService INSTANCE;
    public synchronized static LoggerService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new LoggerService();
        }

        return INSTANCE;
    }
}
