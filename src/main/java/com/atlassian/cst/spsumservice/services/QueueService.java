package com.atlassian.cst.spsumservice.services;

import com.atlassian.cst.spsumservice.pojo.QueueMessageStoryPoints;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.SendMessageRequest;
import software.amazon.awssdk.services.sqs.model.SendMessageResponse;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * QueueService
 * Service bean providing an API to push the summation results to a configured SQS queue
 * Set the QUEUE_URL environment variable to configure for a remote endpoint
 */
@Service
public class QueueService {

    private static final String QUEUE_URL_ENV_VAR_NAME = "QUEUE_URL";
    private static final String DEFAULT_QUEUE_URL = "http://localhost:9324/queue/cst-test-queue";
    private static final LoggerService logger = LoggerService.getInstance();
    private SqsClient sqsClient;

    private static String getQueueUrl() {
        String queueUrl = System.getenv(QUEUE_URL_ENV_VAR_NAME);
        if (queueUrl == null) {
            logger.warn("QUEUE_URL environment variable not defined. Assuming default...");
            return DEFAULT_QUEUE_URL;
        }

        return queueUrl;
    }

    // Designed as a singleton. Use getInstance() instead.
    public QueueService() {
        try {
            sqsClient = SqsClient.builder().endpointOverride(new URI(getQueueUrl())).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param descriptive_name The value of the name field used in the message pushed to the queue
     * @param story_points The value of the totalPoints field used in the message pushed to the queue
     * @return true if successful, false if unsuccessful
     */
    public boolean pushStoryPointSumToQueue(String descriptive_name, int story_points) {
        try {
            QueueMessageStoryPoints queueMessageObj = new QueueMessageStoryPoints();
            queueMessageObj.setName(descriptive_name);
            queueMessageObj.setTotalPoints(story_points);

            long startTime = System.nanoTime();

            String queueMessageJson = new ObjectMapper().writeValueAsString(queueMessageObj);
            logger.info("Queuing message: " + queueMessageJson);
            SendMessageRequest send_msg_request = SendMessageRequest.builder()
                .queueUrl(getQueueUrl())
                .messageBody(queueMessageJson)
                .build();

            SendMessageResponse sendMessageResponse = sqsClient.sendMessage(send_msg_request);

            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;

            SdkHttpResponse sdkHttpResponse = sendMessageResponse.sdkHttpResponse();
            logger.logQueueResponse(timeElapsed, sdkHttpResponse.statusCode());
            return sdkHttpResponse.isSuccessful();

        } catch (Exception e) {
            logger.logQueuingException(e);
            return false;
        }
    }
}
