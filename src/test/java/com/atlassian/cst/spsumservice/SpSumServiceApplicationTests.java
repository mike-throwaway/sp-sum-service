package com.atlassian.cst.spsumservice;

import com.atlassian.cst.spsumservice.pojo.JiraIssue;
import com.atlassian.cst.spsumservice.pojo.JiraIssues;
import com.atlassian.cst.spsumservice.services.JiraService;
import com.atlassian.cst.spsumservice.services.QueueService;
import org.junit.Assert;
import org.junit.AssumptionViolatedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpSumServiceApplicationTests {

	@Autowired
	private SpSumServiceApplication controller;

	@MockBean
	private JiraService jiraServiceMock;

	@MockBean
	private QueueService queueServiceMock;

	@Test
	public void shouldFailGracefullyOnInvalidQuery() {
		Mockito.when(jiraServiceMock.search(anyString()))
				.thenThrow(new AssumptionViolatedException("Search called unexpectedly"));

		Mockito.when(queueServiceMock.pushStoryPointSumToQueue(anyString(), anyInt()))
				.thenThrow(new AssumptionViolatedException("Queue push called unexpectedly"));

		ResponseEntity<String> responseNull = controller.sum(null, "Descriptive...");
		Assert.assertEquals(responseNull.getStatusCode(), HttpStatus.BAD_REQUEST);


		ResponseEntity<String> responseEmpty = controller.sum("", "Descriptive...");
		Assert.assertEquals(responseEmpty.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void shouldFailGracefullyOnInvalidDescription() {
		Mockito.when(jiraServiceMock.search(anyString()))
				.thenThrow(new AssumptionViolatedException("Search called unexpectedly"));

		Mockito.when(queueServiceMock.pushStoryPointSumToQueue(anyString(), anyInt()))
				.thenThrow(new AssumptionViolatedException("Queue push called unexpectedly"));

		ResponseEntity<String> responseNull = controller.sum("test123", null);
		Assert.assertEquals(responseNull.getStatusCode(), HttpStatus.BAD_REQUEST);

		ResponseEntity<String> responseEmpty = controller.sum("test123", "");
		Assert.assertEquals(responseEmpty.getStatusCode(), HttpStatus.BAD_REQUEST);
	}

	@Test
	public void shouldHandleMalformedJsonJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("Text not json");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandleEmptyArrayJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[]");
		Assert.assertEquals(jiraServiceMock.search("test").size(), 0);
	}

	@Test
	public void shouldHandleNonArrayJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("{}");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandlBadArrayItemJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[123]");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandlMissingFieldsJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[{\"issueKey\": \"test\"}]");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandleWrongFieldsTypeJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[{\"issueKey\": \"test\", \"fields\": 123}]");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandleMissingStoryPointFieldJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[{\"issueKey\": \"test\", \"fields\": {}}]");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void shouldHandleWrongStoryPointFieldTypeJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[{\"issueKey\": \"test\", \"fields\": {\"storyPoints\": \"fail\"}}]");
		Assert.assertNull(jiraServiceMock.search("test"));
	}

	@Test
	public void testValidJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[{\"issueKey\": \"test\", \"fields\": {\"storyPoints\": 11}}]");
		JiraIssues jiraIssues = jiraServiceMock.search("test");
		Assert.assertEquals(jiraIssues.size(), 1);

		JiraIssue jiraIssue = jiraIssues.get(0);
		Assert.assertEquals(jiraIssue.getIssueKey(), "test");
		Assert.assertEquals(jiraIssue.getStoryPoints(), 11);

	}

	@Test
	public void testCountJiraResponse() {
		Mockito.when(jiraServiceMock.search(anyString())).thenCallRealMethod();
		Mockito.when(jiraServiceMock.rawSearch(any())).thenReturn("[" +
				"{\"issueKey\": \"test-1\", \"fields\": {\"storyPoints\": 11}}," +
				"{\"issueKey\": \"test-2\", \"fields\": {\"storyPoints\": 5}}]");

		JiraIssues jiraIssues = jiraServiceMock.search("test");
		Assert.assertEquals(jiraIssues.getTotalStoryPoints(), 16);
	}
}

